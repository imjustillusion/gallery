"use strict";

import $ from 'jquery'
import Hogan from 'hogan.js'

// reuire hogan.js and lodash
export default class {
  constructor(items, options = {}, ) {
    if (items.length == 0){
      return console.warn('image list cannot be empty');
    }
    // set options
    this.options = Object.assign({
      'element': 'gallery',
      'loop' : true,
      //tpls
      'tpl.main' : '<div class="main"><img src="{{main}}"></div>',
      'tpl.galleryWrap': '{{{main}}} {{{thumbs}}} {{{buttons}}}',
      'tpl.thumbWrap': '<div class="thumbs">{{{output}}}</div>',
      'tpl.thumbItem': '<div class="{{class}}"><img src="{{thumb}}"></div>',
      'tpl.next' : '<button class="next">&rarr;</button>',
      'tpl.prev' : '<button class="prev">&larr;</button>',
      'tpl.fullscreen' : '<button class="fullscreen" title="fullscreen"><span>+</span></button>',
      // classes
      'thumbClass': 'thumb',
      'mainClass': 'main',
      'storage_key': 'glr',
    },
      options);
    this.items = items;
  }

  render(){
    var tpl = Hogan.compile(this.options['tpl.galleryWrap']);
    //render gallery
    var thumbs = this.rednerThumbs();
    var main = this.renderMain();
    var buttons = Hogan.compile(this.options['tpl.prev'] + this.options['tpl.next'] + this.options['tpl.fullscreen']).render();
    this.addListeners();
    //render into dom
    $('#'+this.options['element']).append(tpl.render({main:main, thumbs:thumbs, buttons: buttons}))
  }

  // main item
  renderMain(){
    var tpl = Hogan.compile(this.options['tpl.main']);
     // get current or first element
    var key = this.current || 0;
    var item = this.items[key];
    return tpl.render({main: item});
  }

  reRenderMain(){
    var item = this.renderMain();
    //replace main by $
    $('.'+this.options['mainClass'],'#'+this.options['element']).replaceWith(item);
  }

  rednerThumbs(){
    var thumbs = [];
    var tpl = Hogan.compile(this.options['tpl.thumbItem']);
    var tplWrap = Hogan.compile(this.options['tpl.thumbWrap']);
    this.items.map((value, key)=>{
      thumbs.push(tpl.render({thumb: value, 'class': this.options['thumbClass']}));
    });
    return tplWrap.render({output: thumbs.join(' ')});
  }

  // add click listeners on elements
  addListeners(){
    $(document).on('click', '#'+this.options['element']+' .'+this.options['thumbClass'], (e)=>{
      this.current = $(e.currentTarget).index();
      this.reRenderMain();
    })
    $(document).on('click', '#'+this.options['element']+' button', (e)=>{
      if ($(e.currentTarget).hasClass('next')){
        this.next();
      } else if ($(e.currentTarget).hasClass('prev')) {
        this.prev();
      }
      else if ($(e.currentTarget).hasClass('fullscreen')){
        this.fullscreen();
      }
    })
    $(document).on('keyup', (e)=>{
      if (e.keyCode == 27 && $('#'+this.options['element']).hasClass('full')){
        $('#'+this.options['element']).removeClass('full');
      }
    })
  }

    // get current item
    get current() {
      return this.storage('current') || false;
    }

    // set current
    set current(itemNum) {
      this.storage('current',itemNum);
    }
    // go next item
    next(){
      if (this.items[parseInt(this.current)+1]){
        this.current = parseInt(this.current)+1;
      } else {
        if (this.options['loop'] == true){
          this.current = 0;
        }
      }
      this.reRenderMain();
    }
    // go prev item
    prev(){
      if (this.options['loop'] && this.current == 0)
        this.current = this.items.length-1;
      else {
        this.current = parseInt(this.current)-1;
      }
      this.reRenderMain();
    }
    //go to fullscreen mode
    fullscreen(){
      if (!document.fullscreenElement &&    // alternative standard method
          !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
        if (document.getElementById(this.options['element']).requestFullscreen) {
          document.getElementById(this.options['element']).requestFullscreen();
        } else if (document.getElementById(this.options['element']).mozRequestFullScreen) {
          document.getElementById(this.options['element']).mozRequestFullScreen();
        } else if (document.getElementById(this.options['element']).webkitRequestFullscreen) {
          document.getElementById(this.options['element']).webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        $('#'+this.options['element']).addClass('full');
      } else {
        if (document.cancelFullScreen) {
          document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
          document.webkitCancelFullScreen();
        }
        $('#'+this.options['element']).removeClass('full');
      }
    }


    storage(key, value = false){
      if (value === false)
        return localStorage[this.options['storage_key']+'.'+ key];
      localStorage[this.options['storage_key']+'.'+ key] = value;
    }

}
