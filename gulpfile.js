'use strict';

var gulp = require('gulp'),
  sass = require('gulp-sass'),
  livereload = require('gulp-livereload'),
  connect = require('gulp-connect'),
  gutil = require('gulp-util'),
  jade = require('gulp-jade'),
  notify = require("gulp-notify"),
  webpack = require("webpack");

// connect
gulp.task('connect', function() {
  connect.server({
    root: 'app',
    livereload: true
  });
});

// precompile html
gulp.task('html', function() {
  //gulp.src('./build/tpl/*.haml')
  //  .pipe(haml())
  //  .pipe(gulp.dest('./app'))
  //  .pipe(notify('reload html'))
  //  .pipe(connect.reload())
  gulp.src('./build/tpl/*.jade')
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest('./app'))
    .pipe(connect.reload())
    .pipe(notify('reload html'))
});

// precompile js
gulp.task("js", function(callback) {
  // run webpack
  webpack({
    entry: './build/scripts/app',
    devtool: "source-map",
    output: {
      path: __dirname + '/app/scripts/',
      filename: 'app.js',
      library: 'app'
    },
    module: {
      loaders: [{
        test: /\.js$/,
        loader: "babel-loader",
        query: {
          cacheDirectory: true,
          presets: ['es2015', 'stage-0']
        }
      }]
    }
  }, function(err, stats) {
    if (err) throw new gutil.PluginError("webpack", err);
    gutil.log("[webpack]", stats.toString({
      // output options
    }));
    callback();
  });
  gulp.src('./build/scripts/**/*.js').pipe(notify('webpack compile'))
});

//precompile css
gulp.task('css', function() {
  gulp.src('./build/styles/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'))
    .pipe(notify('reload css'))
    .pipe(connect.reload())
    //.pipe(notify('Sass compiled.'));
});

gulp.task('watch', function() {
  gulp.watch('build/styles/**/*.scss', ['css'])
  gulp.watch('build/scripts/**/*.js', ['js'])
  gulp.watch('build/tpl/**/*.jade', ['html'])
});


gulp.task('default', ['connect', 'html', 'js', 'css', 'watch']);
