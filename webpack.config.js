'use strict';

module.exports = {
    entry: './build/scripts/app',
    devtool: "source-map",
    output: {
      path: __dirname + '/app/js/',
      filename: 'app.js',
      library: 'app'
    },
    module: {
       loaders: [
         {
          test: /\.js$/,
          loader: "babel-loader",
          query: {
            cacheDirectory: true,
            presets: ['es2015']
          }
        }
      ]
     }
  }
